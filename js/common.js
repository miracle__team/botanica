$(document).ready(function() {
  
  //Burger menu
  $('.burger-wrap').click(function(){
    $(this).children().toggleClass('active');
    $(this).next().toggleClass('active');
    return false;
  });
  
  // Select2
  
  $('.select').select2();
  
  // Fancybox
  
  $('[data-fancybox]').fancybox({
	protect: true
  });
  
  // Radio buttons
  
  $('.order-date').click(function() {
    $('.order-date').each(function() {
      if( $(this).find('input').prop('checked') ) {
        $(this).addClass('active');
      }
      else {
        $(this).removeClass('active');
      }
    });
  });
  
  // Cabinet tabs
  
  $('.cabinet-header-item').click(function() {
    
    $('.cabinet-header-item').removeClass('active');
    $(this).addClass('active');
    
    $('.cabinet-items').removeClass('active');
    $('.cabinet-items').eq($(this).index()).addClass('active');
    
  });
  
  
});





